package com.arquitecturajava.springbootthymeleaf.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/people")
public class PersonController {

  @RequestMapping("/list")
  public String showPeople(Model model){

    return "people/list.html";

  }

}
